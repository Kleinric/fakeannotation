import sys
from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *
from PyQt4.QtGui import (QWidget,
                         QApplication,
                         QLabel,
                         QPushButton,
                         QIcon,
                         QHBoxLayout,
                         QVBoxLayout,
                         QProgressBar,
                         QRadioButton,
                         QLineEdit,
                         QAction, 
                         QButtonGroup)
import pickle
import sys
import os
import pickle
import glob
import re
from collections import defaultdict
import random
import cv2
import numpy as np

matcher = re.compile('femo_aligned_faces_([0-9]+)_([0-9]+).pkl')
matcher2= re.compile('(.*)/([^\./]+)\.[Mm][Pp]4')

emotions = ['&1. Angry','&2. Contempt','&3. Disgust','&4. Happy','&5. Sad','&6. Surprised']
emo_keys = [QtCore.Qt.Key_1,
            QtCore.Qt.Key_2,
            QtCore.Qt.Key_3,
            QtCore.Qt.Key_4,
            QtCore.Qt.Key_5,
            QtCore.Qt.Key_6]

app = QApplication(sys.argv)

INSTRUCTIONS='''You are about to watch videos of people while showing various expressions on their face. Each of these facial expressions corresponds to one of the following emotions and only one: Happy, Angry, Sad, Surprised, Disgust and Contempt (the feeling that  a thing is worthless or beneath consideration). In one case the emotional state of the subject matches the facial expression we have asked them to perform (they feel Happy and we ask them to act Happy). We will call these TRUE facial expressions. In another case it doesn't (they feel Sad and we ask them to act Happy). We will call these FAKE facial expressions. 
 
In order to create the TRUE and FAKE scenario we do the following. First we show subjects videos in order to make them feel a certain emotion. Then we ask them to act the corresponding facial expression. This expression will be the TRUE expression. In the FAKE scenario we take the subjects by surprise and ask them to act an expression that's different from the emotion we have induced. As they are taken by surprise and they do not feel what they are requested to express, subjects try to fake the facial expression.  

We want to see if you can tell which of the facial expressions was TRUE and which was FAKE. You will have the possibility to pause and replay the video if needed, so take your time. After making your mind you'll have to choose between 12 options (True Happy, Fake Happy, True Angry, Fake Angry, True Sad, Fake Sad, True Surprised, Fake Surprised, True Disgust, Fake Disgust, True Contempt, Fake Contempt), basically having to recognize the emotion and if it was TRUE or FAKE.
'''

IM_SIZE=(500,500)


def dv():
    return (None, None)
   
labels = {
    "N2H" : 0,
    "N2S" : 1,
    "N2D" : 2,
    "N2A" : 3,
    "N2C" : 4,
    "N2Sur" : 5,
    "H2N2S" : 6,
    "H2N2D" : 7,
    "H2N2A" : 8,
    "H2N2C" : 9,
    "D2N2Sur" : 10,
    "S2N2H" : 11,
    "n2h" : 0,
    "n2s" : 1,
    "n2d" : 2,
    "n2a" : 3,
    "n2c" : 4,
    "n2Sur" : 5,
    "h2n2s" : 6,
    "h2n2d" : 7,
    "h2n2a" : 8,
    "h2n2c" : 9,
    "d2n2sur" : 10,
    "s2n2h" : 11    
}

def emo_to_label(path):
    name = matcher2.match(path)
    assert(name is not None)
    try:
        out = labels[name.group(2)]
        assert(out is not None)
        return out
    except KeyError, e:
        print 'I got a KeyError - reason "%s" - name: "%s"' % (str(e), name.group(0))

    
class NativeVideo:
    def preprocess(self, img):
        out = cv2.resize(img, IM_SIZE)
        #out = np.transpose(out, (2, 0, 1))
        return out
  
    def __init__(self, base, path):
        self.path = path
        self.base = base
        #print("Path: %s\nBase: %s", (path, base))
        self.cap = cv2.VideoCapture(self.base + self.path)
        #cv2.cv.CV_CAP_PROP_FRAME_COUNT
        #cv2.CAP_PROP_FRAME_COUNT
        self.n_frames = int(self.cap.get(7))
        self.h, self.w = IM_SIZE
        self.c = 3
        frames = []
        while(1):
            ret, frame = self.cap.read()
            if(not ret):
                break
            frames.append(self.preprocess(frame))
            
        name = matcher2.match(self.path)
        self.subject = "Subject"
        if name is not None:
            self.subject = name.group(1)
        self.video = emo_to_label(self.path)   
        
        if not len(frames) == self.n_frames:
            print("Error in video: %s" % self.path)
            print("Reported %d frames, but only read %d" % (self.n_frames, len(frames)))
            self.n_frames = 1
            self.data = [np.zeros((IM_SIZE[0], IM_SIZE[1], 3), dtype=np.uint8)]
        else:    
            self.data = frames

        
class NativeVideoList:
    folder = "FakeEmotion_Videos"
    info = {'name' : None, 'age' : None, 'gender' : None, 'background' : None}
    def __init__(self, base):
        self.base = base
        self.files = glob.glob(base + '*/*.MP4')
        self.files = [x[len(base):] for x in self.files]
        # Make sure we always get the same 'random' shuffle
        self.files = sorted(self.files)
        random.shuffle(self.files, lambda: 0.42)
        self.n_files = len(self.files)
        self.values = defaultdict(dv)
        
    def get_video(self, idx):
        if idx >= self.n_files:
            return None
        
        return NativeVideo(self.base, self.files[idx])
    
    def set_value(self, idx, emotion, rf):
        self.values[self.files[idx]] = (emotion, rf)
        
    def get_value(self, idx):
        return self.values[self.files[idx]]
    
    def get_prev_blank(self, curr):
        for idx in range(curr - 1, -1, -1):
            c_em, c_rf = self.get_value(idx)
            if c_em is None or c_rf is None:
                return idx
        
        for idx in range(self.n_files - 1, curr, -1):
            c_em, c_rf = self.get_value(idx)
            if c_em is None or c_rf is None:
                return idx
            
        return None
    
    def get_next_blank(self, curr):
        for idx in range(curr + 1, self.n_files):
            c_em, c_rf = self.get_value(idx)
            if c_em is None or c_rf is None:
                return idx
        
        for idx in range(0, curr):
            c_em, c_rf = self.get_value(idx)
            if c_em is None or c_rf is None:
                return idx
            
        return None
    
    def save(self, path):
        print("Save %s" % path)
        f = open(path, 'wb')
        pickle.dump(self, f)
        f.close()
        return True
        
class Video:
    def __init__(self, base, path):
        self.path = path
        self.base = base
        f = open(self.base + self.path, 'rb')
        self.data = pickle.load(f)['faces']
        f.close()
        self.n_frames, self.h, self.w, self.c = self.data.shape
        m = matcher.match(path)
        assert(m is not None)
        self.subject = m.group(1)
        self.video = m.group(2)
        
class VideoList:
    folder = "fake_aligned_color"
    info = {'name' : None, 'age' : None, 'gender' : None, 'background' : None}
    def __init__(self, base):
        self.base = base
        self.files = glob.glob(base + 'femo_aligned_faces_*_*.pkl')
        self.files = [x[len(base):] for x in self.files]
        # Make sure we always get the same 'random' shuffle
        self.files = sorted(self.files)
        random.shuffle(self.files, lambda: 0.42)
        self.n_files = len(self.files)
        self.values = defaultdict(dv)
        
    def get_video(self, idx):
        if idx >= self.n_files:
            return None
        
        return Video(self.base, self.files[idx])
    
    def set_value(self, idx, emotion, rf):
        self.values[self.files[idx]] = (emotion, rf)
        
    def get_value(self, idx):
        return self.values[self.files[idx]]
    
    def get_prev_blank(self, curr):
        for idx in range(curr - 1, -1, -1):
            c_em, c_rf = self.get_value(idx)
            if c_em is None or c_rf is None:
                return idx
        
        for idx in range(self.n_files - 1, curr, -1):
            c_em, c_rf = self.get_value(idx)
            if c_em is None or c_rf is None:
                return idx
            
        return None
    
    def get_next_blank(self, curr):
        for idx in range(curr + 1, self.n_files):
            c_em, c_rf = self.get_value(idx)
            if c_em is None or c_rf is None:
                return idx
        
        for idx in range(0, curr):
            c_em, c_rf = self.get_value(idx)
            if c_em is None or c_rf is None:
                return idx
            
        return None
    
    def save(self, path):
        print("Save %s" % path)
        f = open(path, 'wb')
        pickle.dump(self, f)
        f.close()
        return True
    
class InfoWindow(QDialog):
    def __init__(self):
        super(InfoWindow, self).__init__()
        self.resize(800,600)
        self.info = {'name' : None, 'age' : None, 'gender' : None, 'background' : None}
        l_main = QVBoxLayout()
        l_main.addWidget(QLabel("Name"))
        self.name = QLineEdit()
        l_main.addWidget(self.name)
        l_main.addWidget(QLabel("Age"))
        self.age = QLineEdit()        
        l_main.addWidget(self.age)
        l_main.addWidget(QLabel("Gender"))
        self.gender = QComboBox()
        self.gender.addItem('Male')
        self.gender.addItem('Female')
        l_main.addWidget(self.gender)
        self.background = QTextEdit()        
        l_main.addWidget(QLabel("Background"))
        l_main.addWidget(self.background)
        l_buttons = QHBoxLayout()
        self.cancel = QPushButton('Cancel')
        l_buttons.addWidget(self.cancel)        
        self.ok = QPushButton('Ok')
        l_buttons.addWidget(self.ok)

        l_main.addStretch()
        l_main.addLayout(l_buttons)
        #central = QWidget()
        #central.setLayout(l_main)
        self.ll_h = QHBoxLayout()
        text = QTextEdit()
        text.setText(INSTRUCTIONS)
        text.setReadOnly(True)
        self.ll_h.addWidget(text)
        self.ll_h.addLayout(l_main)
        
        self.ll_h.setStretchFactor(text, 2)
        self.ll_h.setStretchFactor(l_main, 1)
        
        self.setLayout(self.ll_h)
        
        self.ok.clicked.connect(self.on_ok)
        self.cancel.clicked.connect(self.on_cancel)
        
    def setInfo(self, info):
        if info is None:
            return
        self.info = info
        if info['name'] is not None:
            self.name.setText(self.info['name'])
        if info['age'] is not None:
            self.age.setText(self.info['age'])
            
        if self.info['gender'] is not None:
            self.gender.setCurrentIndex(self.info['gender'])
        else:
            self.gender.setCurrentIndex(0)
            
        if self.info['background'] is not None:
            self.background.setText(self.info['background'])
    
    def on_ok(self):
        self.info = {'name' : self.name.text(), 'age' : self.age.text(), 'gender' : self.gender.currentIndex(), 'background' : self.background.toPlainText()}
        self.hide()
            
    def on_cancel(self):
        self.setInfo(self.info)
        self.hide()
    
#def get_video_list(path):
#    f = open(path, 'rb')
#    out = pickle.load(f)
#    f.close()
#    if not os.path.isdir(out.base):
#        fileName = QFileDialog.getExistingDirectory(None, "Test")
#        
#    return out

def new_video_list(path, save_name):
    vl = VideoList(path)
    vl.save(save_name)
    return vl

def new_native_video_list(path, save_name):
    vl = NativeVideoList(path)
    vl.save(save_name)
    return vl
    
class ClickableLineEdit(QLineEdit):
    clicked = QtCore.pyqtSignal() # signal when the text entry is left clicked

    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton: self.clicked.emit()
        else: super().mousePressEvent(event)
        
class MyProgressBar(QProgressBar):
    def __init__(self):
        QProgressBar.__init__(self)
        self.setRange(0, 0)
        self.setAlignment(QtCore.Qt.AlignCenter)
        self._text = None
    
    def setText(self, text):
        self._text = text
    
    def text(self):
        return "%d / %d" % (self.value(), self.maximum())
        
class Window(QMainWindow):
    CONTROL_PLAY = 0
    CONTROL_PAUSE = 1
    
    video_list = None
    idx_emotion = None
    idx_rf = None
    frame = 0
    status = CONTROL_PAUSE
    video = None
    fps = 25
    
    info_window = None
    info = None
    
    def __init__(self):
        super(Window, self).__init__(None)
        
        self.info_window = InfoWindow()
        
        self.setGeometry(100,100,200,100)
        self.setWindowTitle("Annotate")
        l_main = QHBoxLayout()
        
        l_main_left = QVBoxLayout()
        l_main_right = QHBoxLayout()
        l_main_full_r = QVBoxLayout()
        l_main_vid = QHBoxLayout()
        l_main_full_r.addLayout(l_main_right)
        l_main_full_r.addLayout(l_main_vid)
        
        l_main.addLayout(l_main_left)
        l_main.addLayout(l_main_full_r)
        
        self.image = QLabel()
        self.image.setScaledContents(True)
        self.progress = MyProgressBar()
        self.progress.setValue(0)
        self.progress.setMaximum(1)
        l_video_control = QHBoxLayout()
        
        self.control_begin = QPushButton()
        self.control_begin.setToolTip("Begin")
        self.control_begin.setIcon(QIcon.fromTheme("media-skip-backward"))
        self.control_begin.clicked.connect(self.on_begin)
        
        self.control_slower = QPushButton()
        self.control_slower.setToolTip("Skip Back (ALT+Left)")
        self.control_slower.setIcon(QIcon.fromTheme("media-seek-backward"))
        self.control_slower.clicked.connect(self.on_slow)
        
        self.control_pp = QPushButton()
        self.control_pp.setToolTip("Play/Pause (Space)")
        self.control_pp.setIcon(QIcon.fromTheme("media-playback-start"))
        self.control_pp.clicked.connect(self.on_pp)
        self.control_pp.setCheckable(True)
        self.connect(QtGui.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Space), self), QtCore.SIGNAL('activated()'), self.control_pp.click)
        
        self.control_faster = QPushButton()
        self.control_faster.setToolTip("Skip Forward (ALT+Right)")
        self.control_faster.setIcon(QIcon.fromTheme("media-seek-forward"))
        self.control_faster.clicked.connect(self.on_fast)
        
        self.control_end = QPushButton()
        self.control_end.setToolTip("End")
        self.control_end.setIcon(QIcon.fromTheme("media-skip-forward"))
        self.control_end.clicked.connect(self.on_end)
        
        self.connect(QtGui.QShortcut(QtGui.QKeySequence(QtCore.Qt.ALT + QtCore.Qt.Key_Right), self), QtCore.SIGNAL('activated()'), self.control_faster.click)
        self.connect(QtGui.QShortcut(QtGui.QKeySequence(QtCore.Qt.ALT + QtCore.Qt.Key_Left), self), QtCore.SIGNAL('activated()'), self.control_slower.click)
        
        l_video_control.addWidget(self.control_begin)
        l_video_control.addWidget(self.control_slower)
        l_video_control.addWidget(self.control_pp)
        l_video_control.addWidget(self.control_faster)
        l_video_control.addWidget(self.control_end)
        
        l_main_left.addWidget(self.image)
        l_main_left.addWidget(self.progress)
        l_main_left.addLayout(l_video_control)
        
        l_emotions = QVBoxLayout()
        l_rf = QVBoxLayout()
        
        self.emo_buttons = []
        self.emo_group = QButtonGroup()
        for idx in range(len(emotions)):
            e = emotions[idx]
            tmp = QRadioButton(e)
            self.emo_buttons.append(tmp)
            tmp.clicked.connect((lambda a: lambda: self.on_emotion_click(a))(idx))
            self.connect(QtGui.QShortcut(QtGui.QKeySequence(emo_keys[idx]), self), QtCore.SIGNAL('activated()'), tmp.click)
            l_emotions.addWidget(tmp)
            self.emo_group.addButton(tmp)
        
        l_emotions.addStretch()
        w_emotions = QWidget()
        w_emotions.setLayout(l_emotions)
        self.rf_group = QButtonGroup()
        c_real = QRadioButton("&Real")
        c_real.clicked.connect((lambda a: lambda: self.on_rf_click(a))(0))
        self.connect(QtGui.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_R), self), QtCore.SIGNAL('activated()'), c_real.click)
        c_fake = QRadioButton("&Fake")
        c_fake.clicked.connect((lambda a: lambda: self.on_rf_click(a))(1))
        self.connect(QtGui.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_F), self), QtCore.SIGNAL('activated()'), c_fake.click)
        l_rf.addWidget(c_real)
        l_rf.addWidget(c_fake)
        self.rf_group.addButton(c_real)
        self.rf_group.addButton(c_fake)
        self.rf_buttons = [c_real, c_fake]
        l_rf.addStretch()
        w_rf  = QWidget()
        w_rf.setLayout(l_rf)
        
        self.prev_vid = QPushButton()
        self.prev_vid.setText('<')
        self.prev_vid.setToolTip('Previous Video (Left)')
        self.prev_vid.clicked.connect(self.on_prev_video)
        
        self.vid_info = ClickableLineEdit()
        self.vid_info.clicked.connect(self.on_jump_video)
        #self.vid_info.setEnabled(False)
        self.vid_info.setReadOnly(True)
        self.next_vid = QPushButton()
        self.next_vid.setText('>')
        self.next_vid.setToolTip('Next Video (Right)')
        self.next_vid.clicked.connect(self.on_next_video)
        self.next_blank_vid = QPushButton()
        self.next_blank_vid.setText('>|')
        self.next_blank_vid.setToolTip('Next Unlabelled Video (CTRL+Right)')
        self.next_blank_vid.clicked.connect(self.on_next_blank_video)
        self.prev_blank_vid = QPushButton()
        self.prev_blank_vid.setText('|<')
        self.prev_blank_vid.setToolTip('Previous Unlabelled Video (CTRL+Left)')
        self.prev_blank_vid.clicked.connect(self.on_prev_blank_video)
        
        self.connect(QtGui.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Right), self), QtCore.SIGNAL('activated()'), self.next_vid.click)
        self.connect(QtGui.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Left), self), QtCore.SIGNAL('activated()'), self.prev_vid.click)
        self.connect(QtGui.QShortcut(QtGui.QKeySequence(QtCore.Qt.CTRL + QtCore.Qt.Key_Left), self), QtCore.SIGNAL('activated()'), self.prev_blank_vid.click)
        self.connect(QtGui.QShortcut(QtGui.QKeySequence(QtCore.Qt.CTRL + QtCore.Qt.Key_Right), self), QtCore.SIGNAL('activated()'), self.next_blank_vid.click)

        l_main_vid.addWidget(self.prev_blank_vid)
        l_main_vid.addWidget(self.prev_vid)
        l_main_vid.addWidget(self.vid_info)
        l_main_vid.addWidget(self.next_vid)
        l_main_vid.addWidget(self.next_blank_vid)
        
        l_main_right.addWidget(w_emotions)
        l_main_right.addWidget(w_rf)
        
        #self.menu = QMenuBar(self)
        self.menu = self.menuBar()
        file = self.menu.addMenu('&File')
        info = QAction('MyInfo',self)
        file.addAction(info)
        info.activated.connect(self.on_info)
        open = QAction('Open', self)
        file.addAction(open)
        open.activated.connect(self.open)
        save = QAction('Save', self)
        file.addAction(save)
        save.activated.connect(self.save_list)
        exit = QAction('Exit', self)
        file.addAction(exit)    
        exit.activated.connect(self.close)
        self.connect(QtGui.QShortcut(QtGui.QKeySequence(QtCore.Qt.CTRL + QtCore.Qt.Key_O), self), QtCore.SIGNAL('activated()'), open.trigger)
        self.connect(QtGui.QShortcut(QtGui.QKeySequence(QtCore.Qt.CTRL + QtCore.Qt.Key_S), self), QtCore.SIGNAL('activated()'), save.trigger)
        central = QWidget()
        central.setLayout(l_main)
        #self.setLayout(l_main)
        self.setCentralWidget(central)
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.tick)
        self.show()
        
    def on_info(self):
        self.info_window.setInfo(self.info)
        self.info_window.exec_()
        self.info = self.info_window.info
        
    def on_begin(self):
        print("on_begin")
        self.set_frame(0)
    
    def on_jump_video(self):
        newF, status = QInputDialog.getInteger(QWidget(), "Jump to Video", "Enter Video Number", self.curr_video+1, 0, self.video.n_frames)
        if status:
            self.set_video_idx(newF-1)
    
    def on_slow(self):
        self.frame = self.frame - 50
        if self.frame < 0:
            self.set_pause()
            self.frame = 0
            return
            
        self.set_frame(self.frame)
    
    def on_pp(self):
        print("on_pp")
        if self.status == self.CONTROL_PLAY:
            self.set_pause()
        else:
            self.set_play()
        pass
    
    def set_play(self):
        print('play')
        if self.video is None:
            return
        
        self.status = self.CONTROL_PLAY
        self.timer.start(1000/self.fps)
        self.control_pp.setChecked(True)

        
    def set_pause(self):
        print('pause')
        self.status = self.CONTROL_PAUSE
        self.timer.stop()
        self.control_pp.setChecked(False)
    
    def tick(self):
        assert(self.status == self.CONTROL_PLAY)
        self.frame = self.frame + 1
        if self.frame >= self.video.n_frames:
            self.set_pause()
            return
            
        self.set_frame(self.frame)
    
    def on_fast(self):
        self.frame = self.frame + 50
        if self.frame >= self.video.n_frames:
            self.set_pause()
            self.frame = self.video.n_frames -1
            return
            
        self.set_frame(self.frame)
    
    def on_end(self):
        print("on_end")
        self.set_frame(self.video.n_frames-1)
        pass
    
    def on_emotion_click(self, idx):
        self.idx_emotion = idx
        print("on_emotion_click: %d" % idx)
        pass
    
    def on_rf_click(self, idx):
        self.idx_rf = idx
        print("on_rf_click: %d" % idx)
        pass
    
    def set_video(self, video):
        print("set_video")
        self.video = video
        self.set_frame(0)
        self.frame = 0
        self.progress.setMaximum(self.video.n_frames)
        pass
    
    def on_next_video(self):
        self.set_video_idx(self.curr_video + 1)
        pass
    
    def on_prev_blank_video(self):
        idx = self.video_list.get_prev_blank(self.curr_video)
        if idx is not None:
            self.set_video_idx(idx)
            
    def on_next_blank_video(self):
        idx = self.video_list.get_next_blank(self.curr_video)
        if idx is not None:
            self.set_video_idx(idx)
            
    
    def on_prev_video(self):
        self.set_video_idx(self.curr_video - 1)
        pass
    
    def write_values(self):
        if self.video_list is not None:
            self.video_list.set_value(self.curr_video, self.idx_emotion, self.idx_rf)
            self.video_list.info = self.info
    
    def set_video_idx(self, idx, write = True):
        if self.video_list is None:
            self.image.setPixmap(QPixmap())
            return
        
        print(idx)
        print(self.video_list.n_files)
        if idx >= self.video_list.n_files:
            idx = 0
            
        if idx < 0:
            idx = self.video_list.n_files - 1
        if write:
            self.write_values()
            
        self.curr_video = idx      
        self.set_video(self.video_list.get_video(self.curr_video))
        self.vid_info.setText("%d / %d" % (self.curr_video+1, self.video_list.n_files))
        emo, rf = self.video_list.get_value(self.curr_video)
        print((emo,rf))
        print("Clearing")
        self.clear_values()
        if emo is not None:
            self.emo_buttons[emo].setChecked(True)
            self.idx_emotion = emo
        if rf is not None:
            self.rf_buttons[rf].setChecked(True)
            self.idx_rf = rf
        pass
    
    def set_frame(self, frame):
        self.frame = frame
        v = self.get_image(frame)
        assert(v is not None)
        self.image.setPixmap(v)
        print('set_frame: %d' % frame)
        self.progress.setValue(frame+1)
        
    def clear_values(self):
        self.emo_group.setExclusive(False)
        for e in self.emo_buttons:
            e.setChecked(False)
        self.emo_group.setExclusive(True)        
        self.rf_group.setExclusive(False)
        for e in self.rf_buttons:
            e.setChecked(False)
        self.rf_group.setExclusive(True)    
        
        self.idx_emotion = None
        self.idx_rf = None            
        
    def get_image(self, frame):
        if self.video is None:
            return None
                    
        if frame >= self.video.n_frames:
            return None
        
        height, width = self.video.h, self.video.w
        bytesPerLine = 3 * width
        qImg = QImage(self.video.data[frame].data, width, height, bytesPerLine, QImage.Format_RGB888).rgbSwapped()
        return QPixmap(qImg)
        
    def set_video_list(self, vlist):
        if self.video_list is not None:
            self.close_list()
        
        if self.video_list is not None:
            return False
        
        self.video_list = vlist
        self.curr_video = 0
        self.set_video_idx(self.curr_video, False)
        self.info = self.video_list.info
    
    def closeEvent(self, event):
        # do stuff
        if self.close_list():
            event.accept()
        else:
            event.ignore()
    
    def close_list(self):
        if self.video_list is None:
            return True
        
        quit_msg = "Save changes?"
        reply = QMessageBox.question(self, 'Message', 
                     quit_msg, QMessageBox.Yes, QMessageBox.No)
        if reply == QMessageBox.Yes:
            if self.save_list():
                self.video_list = None
                self.curr_video = -1
                self.set_video_idx(self.curr_video)
        else:
            self.video_list = None
            self.curr_video = -1
            self.set_video_idx(self.curr_video)
            return True
    
    def save_list(self):
        if self.video_list is None:
            return True
        self.write_values()
        if self.info is None:
            self.on_info()
            
        fileName = QFileDialog.getSaveFileName(self, 'Dialog Title', '.', filter='pkl (*.pkl)')
        if fileName:
            return self.video_list.save(fileName)   
        return False
    
    def open_video_list(self, path):
        f = open(path, 'rb')
        out = pickle.load(f)
        f.close()
        return out

    def open(self):
        #pdb.set_trace()
        self.write_values()
        self.close_list()
        if self.video_list is None:
            fileName = QFileDialog.getOpenFileName(self, 'Dialog Title', '.', filter='pkl (*.pkl)')
            if fileName:
                v = self.open_video_list(fileName)
                if v is not None:
                    if not os.path.isdir(v.base):
                        QMessageBox.warning(self, "Video Folder Missing", "Cannot find video folder, please select it. Should be titled: %s" % v.folder)
                        fileName = str(QFileDialog.getExistingDirectory(None, "Video Folder"))
                        print fileName
                        if fileName[-1] != '/':
                            fileName += '/'
                        v.base = fileName
                        print("Base: %s" % v.base)
                    self.set_video_list(v)
                    self.on_info()
                    return True
        return False
        

#Generate new pkl file
#location = '/media/ric/5TB/3D CNN - Fake_Real Expression/fake_aligned_color/'
#vl = new_video_list(location, 'file.pkl')
#location = '../FakeEmotion_Videos/'
#vl = new_native_video_list(location, 'file.pkl')
 
if __name__ == "__main__":
    WIN = Window()
    sys.exit(app.exec_())
